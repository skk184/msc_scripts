#!/usr/local/bin/python3
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
import csv
import sys
import time
param = __import__('2019_12_11_REIXS_SFO-STO')


fig = plt.figure()
if sys.argv[1] == "loop":
  delay = float(sys.argv[2])
  offset = 3
  while True:
    for index in range(len(sys.argv)-offset):
      ax = plt.axes(projection='3d')
      #ax.view_init(0, -90) #view from th
      ax.view_init(0, 180) #view from tth
      ax.view_init(0, 135) #view from tth
      #ax.xaxis.set_ticks(np.arange(param.TH0_START, param.TH2_END+1, 0.5))
      ax.set_xlabel('th')
      ax.set_ylabel('tth')
      ax.set_zlabel('pd')
      ax.set_zlim([-0.5*10**(-15),5*10**(-15)])
      ax.set_title(sys.argv[index+offset])
      th = []
      tth = []
      pd = []
      reader = csv.reader(open(sys.argv[index+offset]), delimiter=" ")
      for row in reader:
        th.append(row[0])
        tth.append(row[1])
        pd.append(row[2])
      ax.scatter(np.asarray(th).astype(np.float), np.asarray(tth).astype(np.float), np.asarray(pd).astype(np.float), c=np.asarray(pd).astype(np.float), cmap='cividis_r')
      #plt.show() 
      plt.pause(delay)
else:
  offset = 1
  ax = plt.axes(projection='3d')
  ax.view_init(0, -90)
  #ax.view_init(0, 180)
  #ax.xaxis.set_ticks(np.arange(param.TH0_START, param.TH2_END+1, 0.5))
  ax.set_xlabel('th')
  ax.set_ylabel('tth')
  ax.set_zlabel('pd')
  ax.set_zlim([-0.5*10**(-15),5*10**(-15)])
  ax.set_title(sys.argv[1:])
  for index in range(len(sys.argv)-offset):
    th = []
    tth = []
    pd = []
    reader = csv.reader(open(sys.argv[index+offset]), delimiter=" ")
    for row in reader:
      th.append(row[0])
      tth.append(row[1])
      pd.append(row[2])
    ax.scatter(np.asarray(th).astype(np.float), np.asarray(tth).astype(np.float), np.asarray(pd).astype(np.float), c=np.asarray(pd).astype(np.float), cmap='cividis_r')
  plt.show()
