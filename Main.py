#!/usr/local/bin/python3
import SFO_DataAnalysis as da
import matplotlib.pyplot as plt
import numpy as np
#December 11 SFO-LAO.spec 
#December 11 SFO-STO.spec (3 peak)
#January 14 SFO-STO-B.spec
#January 14 SFO-LSAT-T-REMNT.spec
#January 14 SFO-LSAT-B-REMNT.spec
#January 14 SFO-STO-B-RECTR.spec
#January 14 SFO-LSAT-B-Wed.spec
#March 04 SFO-LSAT-B-Magnet.spec
#March 04 SFO-LSAT-B-Magnet2.spec
#August 26 LBSFO13-LSATT.spec

PARAM_FILE = ["2020_08_26_REIXS_SFO-LSAT-T-Magnet_Energy.py"]
files = [
"2020_08_26_REIXS_SFO-LSAT-T-Magnet_Zone1.py",
"2020_08_26_REIXS_SFO-LSAT-T-Magnet_Zone2.py",
#"2020_08_26_REIXS_SFO-LSAT-T-Magnet.py",
"2019_12_11_REIXS_SFO-LAO.py",
"2020_01_14_REIXS_SFO-STO-B-RECTR.py",
"2020_01_14_REIXS_SFO-STO-B.py",
]

meshfiles = [
"2019_12_11_REIXS_SFO-STO.py"
]

#PARAM_FILE = {
#"SENSOR": "SDD",
#"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2019_12_11_REIXS/Data/SFO-LAO.spec",
#"TITLE": "2019_12_11_REIXS_SFO-LAO",
##"OUTPUT_LOCATION": "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2019_12_11_REIXS_SFO-LAO/", 
#"RELEVANT_SCANS": [24,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47],
#"OFFSET_OVERRIDE": 20.4
#}

PARAM_FILE_SDD = [
{
"SENSOR": "SDD_TOT",
"DETECTOR_OVERRIDE": 0,
"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_SDD_Low_Peak_30K",
"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
"RELEVANT_SCANS": [46]
},
{
"SENSOR": "SDD_TOT",
"DETECTOR_OVERRIDE": 0,
"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_SDD_Low_Peak_30K",
"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
"RELEVANT_SCANS": [113]
},
{
"SENSOR": "SDD_TOT",
"DETECTOR_OVERRIDE": 0,
"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_SDD_High_Peak_30K",
"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
"RELEVANT_SCANS": [44]
},
{
"SENSOR": "SDD_TOT",
"DETECTOR_OVERRIDE": 0,
"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_SDD_High_Peak_30K",
"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
"RELEVANT_SCANS": [107]
},
{
"SENSOR": "SDD_TOT",
"DETECTOR_OVERRIDE": 0,
"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_SDD_High_Peak_30K",
"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
"RELEVANT_SCANS": [116]
}
]

#PARAM_FILE_MCP = [
#{
#"SENSOR": "MCP",
#"DETECTOR_OVERRIDE": 9.3,
#"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_MCP_Low_Peak_30K",
#"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
#"RELEVANT_SCANS": [46]
#},
#{
#"SENSOR": "MCP",
#"DETECTOR_OVERRIDE": 9.3,
#"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_MCP_Low_Peak_30K",
#"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
#"RELEVANT_SCANS": [113]
#},
#{
#"SENSOR": "MCP",
#"DETECTOR_OVERRIDE": 9.3,
#"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_MCP_High_Peak_30K",
#"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
#"RELEVANT_SCANS": [44]
#},
#{
#"SENSOR": "MCP",
#"DETECTOR_OVERRIDE": 9.3,
#"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_MCP_High_Peak_30K",
#"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
#"RELEVANT_SCANS": [107]
#},
#{
#"SENSOR": "MCP",
#"DETECTOR_OVERRIDE": 9.3,
#"TITLE": "2020_08_26_REIXS_LSAT-T_Escan_MCP_High_Peak_30K",
#"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT",
#"RELEVANT_SCANS": [116]
#}
#]

datatypes = ["QLen", "FWHM", "Integrals", "TempSeries_Raw", "TempSeries_Fit", "TempSeries_Both", "TempSeries_Raw_Tth"]
#datatypes = ["TempSeries_Fit"]
datatypes = ["TempSeries_Raw_Tth"]
#datatypes = ["TempSeries_Raw"]
#datatypes = ["QLen"]
#datatypes = ["Fluorescence"]


def all_data(files, datatypes):
    #for index in range(len([0])):
    #ax = plt.axes()
    for index in range(len(files)):
        data = da.ThTthDataProcessing(files[index], fit=True)
        #ax.plot(data.temp, data.qlen, marker="o", label=files[index])
        #data.yspace = 1/10
        for index2 in range(len(datatypes)):
            data.ShowPlot(datatypes[index2])
            #data.ShowPlot(datatypes[index2], aspectratio=0.03)
            #data.SavePlot(datatypes[index2])
            #data.SavePlot(datatypes[index2], aspectratio=0.03)
            #data.SaveData(datatypes[index2])
            pass

    for index in range(len(meshfiles)):
        data = da.ThTthMeshDataProcessing(meshfiles[index], fit=False)
        data.ShowPlot(datatype="TempSeries_Raw")
    #    ax.plot(data.temp, data.qlen, marker="o", label=meshfiles[index])
    #ax.set_xlabel("Temperature (K)") 
    #ax.set_ylabel("Peak Location (q)")
    #plt.legend()
    #plt.savefig("SFO-STO-QLen_All.png")

#all_data(files, datatypes)

def energydata(paramfile, datatypes):
    for index in range(len(paramfile)):
        ax = plt.axes()
        data = da.EnergyDataProcessing(paramfile[index])
        data.ShowPlot()
        xlabel = "Energy (eV)"
        ylabel = "SDD Intensity (arb. units)"
        #ylabel = "MCP Intensity (arb. units)"
        #ax.plot(data.energy[0], data.sensor[0]/max(data.sensor[0]), marker="o", label=str(data.temp[0])+str(np.mean(data.tth[0])))
        #ax.plot(data.energy[0], data.sensor[0]/max(data.sensor[0]), label="Tth: "+str(data.sInfo[data.parameters["RELEVANT_SCANS"][0]-1]["TwoTheta"]))
        #ax.plot(data.energy[0], (data.sensor[0]-min(data.sensor[0]))/max(data.sensor[0]), label="Tth: "+str(data.sInfo[data.parameters["RELEVANT_SCANS"][0]-1]["TwoTheta"]))
        #ax.set_xlabel(xlabel)
        #ax.set_ylabel(ylabel)
        #if index == 1 or index == 4:
        #    ax.legend()
        #    #ax.set_title(data.title)
        #    #plt.show()
        #    plt.savefig(data.title)
        #    plt.clf()
    #if index != 1 and index != 4:
    #    ax.legend()
    #    #ax.set_title(data.title)
    #    #plt.show()
    #    plt.savefig(data.title)
    #    plt.clf()


energydata(PARAM_FILE_SDD, datatypes="Fluorescence")

