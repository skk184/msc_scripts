#!/usr/local/bin/python3
import SFO_DataAnalysis as da

files = [
'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_Energy/'
]
data = da.EscanDataProcessing(files[0])
data.Plot(index=0)
