TYPE = "not mesh"
SENSOR = "MCP"

DATA_FILE = "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT"

OUTPUT_PREFIX = "2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP_"

MULTIPLE_ZONES = "true"

ZONE1 = [26, 30]#tth
ZONE2 = [56, 85]#tth

OUTPUT_LOCATION = "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP/" 
OUTPUT_LOCATION_ZONE1 = "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP_ZONE1/" 
OUTPUT_LOCATION_ZONE2 = "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP_ZONE2/" 

RELEVANT_SCANS = [51,52,53,56,63,64,65,66,67,72,73,74,75,76,79,80,81,84,85,86,89,90,92,93,94,95,98,99,100]

#[17.0, 17.0, 17.0, 17.33, 17.33, 17.33, 17.33, 17.33]
