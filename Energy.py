TYPE = "not mesh"
SENSOR = "MCP"
DATA_FILE = "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_08_26_REIXS_Mail-In/Data/LBSFO13-LSATT"

OUTPUT_PREFIX = "2020_08_26_REIXS_SFO-LSAT-T-Magnet_Energy"

MULTIPLE_ZONES = "false"

ZONE1 = [14, 20]#tth
ZONE2 = [45, 85]#tth

OUTPUT_LOCATION = "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_Energy/" 

RELEVANT_SCANS = [8]

