#!/usr/local/bin/python3
import natsort
import csv
import os
import numpy as np
import matplotlib.pyplot as plt


path1 = 'gamma_constrained/'
path2 = 'gamma_unconstrained/'
test_type = "Integrals"

def ReadFiles(files):
    x1 = []
    y1 = []
    reader = csv.reader(open(path1+files), delimiter=" ")
    for row in reader:
        x1.append(float(row[0]))
        y1.append(float(row[1]))
    x2 = []
    y2 = []
    reader = csv.reader(open(path2+files), delimiter=" ")
    for row in reader:
        x2.append(float(row[0]))
        y2.append(float(row[1]))
    return x1, y1, x2, y2

def GetFiles():
    path = 'gamma_constrained/'
    files = natsort.natsorted(os.listdir(path))
    return files

files = GetFiles()
files = [i for i in files if test_type in i]

for index in range(len(files)):
    x1, y1, x2, y2 = ReadFiles(files[index])
    ax = plt.axes()
    ax.plot(x1, y1, label=path1+"(original)")
    ax.plot(x2, y2, label=path2)
    ax.plot(x1, np.asarray(y2)-np.asarray(y1), label='difference')
    ax.legend()
    ax.set_title(files[index])
    #plt.show()
    plt.savefig(files[index][:-4])
    plt.clf()

