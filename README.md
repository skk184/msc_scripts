# msc_scripts

## extracting th/2th data from spec file
Create parameter file (.py) that contains information such as: relevant scans, input file location, output file location, sensor type, etc.Follow the format as indicated in 2019_12_11_REIXS_SFO-LAO.py and other files like it.

Run SFO_ThTthTempSeries_SpecProcessing.py with the parameter file as the only command line input, but exclude the ".py".
Example: SFO_ThTthTempSeries_SpecProcessing.py 2019_12_11_REIXS_SFO-LAO
This will process the spec file and output th/2th data files for each temperature at the specified location as defined in the parameter file.

## analyze th/2th data
Execute main.py as needed. Make adjustments to scripts as needed.
