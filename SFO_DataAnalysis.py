#!/usr/local/bin/python3
from lmfit.models import GaussianModel, VoigtModel, LinearModel, ConstantModel
from SpecFileProcessingUnix import ReadSpecFile
from lmfit import Parameters
from scipy import constants
from scipy import integrate 
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import math
import csv
import natsort
import os
import json
from pathlib import Path
from REIXS_Detector_Offsets import OFFSETS

###########################################################################################################################################
class DataProcessing(object):
    def __init__(self, parampath):
        self._parameters = self._GetParameters(parampath)
        self._CheckParameters()
        self._title = self._GetTitle(parampath)
        self._sInfo, self._sData = ReadSpecFile(self.parameters["DATA_FILE"])

    @property
    def parameters(self):
        return self._parameters

    @property
    def title(self):
        return self._title

    @property
    def sInfo(self):
        return self._sInfo

    @property
    def sData(self):
        return self._sData

# Public methods
####################################################################
    def ElectronVoltToJoule(self, energy):
        return energy/constants.physical_constants["joule-electron volt relationship"][0]

    def EnergyToWavelength(self, energy):
        #energy in eV
        try:
            return constants.c*constants.h/self.ElectronVoltToJoule(energy)
        except ZeroDivisionError:
            return np.inf

    def DegToRad(self, deg):  #math.radians() doesn't work on lists
        return deg*np.pi/180

    def RadToDeg(self, rad): #math.degrees() doesn't work on lists
        return rad*180/np.pi

    def TwoThetaToQ(self, TTH, energy):#output in inverse Angstroms
        return 4*np.pi*np.sin(self.DegToRad(TTH)/2)/self.EnergyToWavelength(energy)*1e-10

    def QToTwoTheta(self, q, energy):#output in degrees
            return self.RadToDeg(np.arcsin(q/1e-10*self.EnergyToWavelength(energy)/(4*np.pi))*2)

    def PrintsData(self):
        print("\n\nFields available in sData:")
        for field in self.sData[self.parameters["RELEVANT_SCANS"][0]-1]: print(field)

    def PrintsInfo(self):
        print("\n\nFields available in sInfo:")
        for field in self.sInfo[self.parameters["RELEVANT_SCANS"][0]-1]: print(field)

# Private methods
####################################################################
    def _ListStringToFloat(self, values):
        return np.asarray(values).astype(np.float)

    def _ListFloatToString(self, values):
        return np.asarray(values).astype(np.string)
    
    def _PlotInitialize(self):
        self._ClearPlot()
        return plt.axes()

    def _ClearPlot(self):
        plt.clf()

    def _GetTitle(self, parampath):
        if "TITLE" in self.parameters:
            title = self.parameters["TITLE"]
        elif type(parampath) == str:
            title = Path(parampath).parts[-1][:-3]
        else:
            print("TITLE empty. You will probably want to add a TITLE to the parameters dict.")
            title = ""
        return title

    def _GetParameters(self, parampath):
        if type(parampath) == dict:
            parameters = parampath
        else:
            with open(parampath, 'r') as file:
                dictstring = file.read().replace('\n', '')
            parameters = json.loads(dictstring)
        return parameters

    def _CheckParameters(self):
        required = ["SENSOR", "DATA_FILE", "RELEVANT_SCANS"]
        for item in required:
            if item not in self.parameters:
                print("ERROR: "+item+" is a mandatory field of the parameter dictionary.")

    def _DataLength(self):
        index = self.parameters["RELEVANT_SCANS"][0]
        length = len(self.sData[index-1]["Sample_T"])
        return length
    
    def _GetDetectorOffset(self):
        if "OFFSET_OVERRIDE" in self.parameters:
            if self.parameters["OFFSET_OVERRIDE"] == "False":
                offset = OFFSETS[self.parameters["SENSOR"]]
            else:
                offset = self.parameters["OFFSET_OVERRIDE"]
        else:
            offset = OFFSETS[self.parameters["SENSOR"]]
        return offset
###########################################################################################################################################
class ThTthDataProcessing(DataProcessing):
    def __init__(self, parampath, fit=True):
        super().__init__(parampath)
        self._energy = self._ReadEnergyData()
        self._temp = self._ReadTempData()
        self._th, self._tth, self._sensor, self._q, self._ymax = self._ReadSeriesData() 
        self._yspace = 1/10 #magic number for series plotting spacing
        if fit == True:
            self._fit, self._fit_params, self._chisqr = self._FitSeriesData()
            self._qlen = self._GetQLen()
            self._fwhm, self.peaks = self._GetFWHMAndPeaks()
            self._integrals = self._GetIntegrals()
    
    @property
    def energy(self):
        return self._energy

    @property
    def ymax(self):
        return self._ymax

    @property
    def temp(self):
        return self._temp

    @property
    def yspace(self):
        return self._yspace

    @yspace.setter
    def yspace(self, yspace):
        print("Setting 'yspace' to ", yspace)
        self._yspace = yspace

    @property
    def th(self):
        return self._th

    @property
    def tth(self):
        return self._tth

    @property
    def sensor(self):
        return self._sensor

    @property
    def q(self):
        return self._q

    @property
    def fit(self):
        return self._fit

    @property
    def fit_params(self):
        return self._fit_params

    @property
    def chi_sqr(self):
        return self._chisqr

    @property
    def qlen(self):
        return self._qlen

    @property
    def fwhm(self):
        return self._fwhm

    @property
    def integrals(self):
        return self._integrals

# Public methods
#############################################################
    def SavePlot(self, datatype, title=None, filename=None, aspectratio='auto'):
        ax = self._PlotInitialize()
        if title == None:
            title = self.title
        if filename == None:
            filename = title + "_" + datatype
        ax.set_title(title)
        self._Plot(ax, datatype, aspectratio)
        plt.savefig(filename)

    def ShowPlot(self, datatype, title=None, aspectratio='auto'):
        ax = self._PlotInitialize()
        if title == None:
            title = self.title
        ax.set_title(title)
        self._Plot(ax, datatype, aspectratio)
        plt.show()

    def SaveData(self, datatype, filename=None):
        if filename == None:
            filename = self.title + "_" + datatype + ".txt"
        if datatype == "Integrals":#Integrals is special because a zero division is possible without this if statement
            x, y = np.mean(self.temp), self.integrals/(max(self.integrals))
            np.savetxt(filename, np.transpose(np.vstack((x, y))))
        else:
            try:
                x, y = {"FWHM": (np.mean(self.temp), self.fwhm), "QLen": (np.mean(self.temp), self.qlen), "TempSeries_Raw_Tth": (self.tth, self.sensor), "TempSeries_Raw": (self.q, self.sensor), "TempSeries_Fit": (self.q, self.fit)}[datatype]
                if datatype=="TempSeries_Raw" or datatype=="TempSeries_Fit" or datatype=="TempSeries_Raw_Tth":#slightly more complicated to store series data
                    with open(filename, 'w') as f:
                        for index in range(len(x)):
                            values = np.transpose(np.vstack((x[index], y[index])))
                            string = ""
                            for index1 in range(len(values)):
                                string = string + "{:e}".format(values[index1][0]) + " " + "{:e}".format(values[index1][1]) + "\n"
                            f.write("%s,\n" % string)
                else:
                    np.savetxt(filename, np.transpose(np.vstack((x, y))))
            except KeyError: 
                if datatype=="TempSeries_Both":
                    print("ERROR in SaveData(): You cannot save both the Raw data and the Fit data to the same file. Try processing and saving the Raw and Fit data separately if you want to save the data.")
                else:
                    print("ERROR in SaveData(): Data type '"+datatype+"' is not a valid option.")

# Private methods
##############################################################
    def _Plot(self, ax, datatype, aspectratio):
        if datatype == "QLen":
            xlabel, ylabel = 'Temperature (K)', 'Peak Location (q)'
            #ax.plot(self.qlen, self._temp, color='r')
            #ax.invert_yaxis()
            ax.plot(self.temp, self.qlen, color='r', marker="o")
            #ax.set(ylim=(min(self._temp), max(self._temp)))
            #ax.set(xlim=(min(self.qlen), max(self.qlen)))
            offset = (max(self.qlen) - min(self.qlen))/15 #magic offset
            ax.set(xlim=(min(self.temp), max(self.temp)))
            ax.set(ylim=(min(self.qlen), max(self.qlen)+offset))

        elif datatype == "FWHM":
            xlabel, ylabel, ylabel2 = 'Temperature (K)', 'FWHM (q)', 'Peak'
            ax2 = ax.twinx()
            #ax2.legend()
            ax.plot(self.temp, self.fwhm, marker="o")
            #ax.plot(self._temp, self.fwhm, label='FWHM', marker="o")
            #ax2.plot(self._temp, self.peaks, color='r', label='Peak', marker="o")
            ax.set(xlim=(min(self.temp), max(self.temp)))
            #ax.set(ylim=(min(self.fwhm), max(self.fwhm)))
            #ax2.set(ylim=(min(self.peaks), max(self.peaks)))
            #ax2.set_ylabel(ylabel2)

        elif datatype == "Integrals":
            xlabel, ylabel = 'Temperature (K)', 'Integrated Intensity (arb. units)'
            ax.plot(self._temp, self.integrals/max(self.integrals), marker="o")
            ax.set(xlim=(min(self.temp), max(self.temp)))
        
        else: #reserved for temperature series options
            xlabel, ylabel = r'|q|(${\AA}^{-1}$)', 'Intensity (arb. units)'
            if datatype == "TempSeries_Both":
                for index in range(len(self.parameters["RELEVANT_SCANS"])):
                    norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
                    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
                    offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
                    ax.plot(self.q[index], self.fit[index]+offset, c=cmap.to_rgba(index + 1))
                    ax.plot(self.q[index], self.sensor[index]+offset, c=cmap.to_rgba(index + 1))
                ax.set(xlim=(min(self.q[0]), max(self.q[0])))
            elif datatype == "TempSeries_Raw":
                for index in range(len(self.parameters["RELEVANT_SCANS"])):
                    norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
                    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
                    offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
                    ax.plot(self.q[index], self.sensor[index]+offset, c=cmap.to_rgba(index + 1))
                ax.set(xlim=(min(self.q[0]), max(self.q[0])))
            elif datatype == "TempSeries_Fit":
                for index in range(len(self.parameters["RELEVANT_SCANS"])):
                    norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
                    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
                    offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
                    ax.plot(self.q[index], self.fit[index]+offset, c=cmap.to_rgba(index + 1))
                ax.set(xlim=(min(self.q[0]), max(self.q[0])))
            elif datatype == "TempSeries_Raw_Tth":
                xlabel, ylabel = r'${2\theta}(^{\circ})$', 'Intensity (arb. units)'
                for index in range(len(self.parameters["RELEVANT_SCANS"])):
                    norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
                    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
                    offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
                    ax.plot(self.tth[index], self.sensor[index]+offset, c=cmap.to_rgba(index + 1))
                ax.set(xlim=(min(self.tth[0]), max(self.tth[0])))
            else:
                print("ERROR: Data type '"+datatype+"' is not a valid option.")
            if aspectratio != 'equal' and aspectratio != 'auto':
                ax.set(ylim=(0, 1+self.yspace*len(self.parameters["RELEVANT_SCANS"])-(aspectratio/self.yspace)))
            else:
                ax.set(ylim=(0, 1+self.yspace*len(self.parameters["RELEVANT_SCANS"])))
            norm = mpl.colors.Normalize(vmin=min(self.temp), vmax=max(self.temp))
            cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
            plt.colorbar(cmap, ticks=np.rint(self._temp)).ax.invert_yaxis()
        ###################
        ax.set_aspect(aspectratio)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        #ax.legend()

    def _GetQLen(self):
        qlen = np.zeros(len(self.parameters["RELEVANT_SCANS"]))
        for index in range(len(self.parameters["RELEVANT_SCANS"])):
            qlen[index] = self._FindLocationOfPeak(self.fit_params[index])
        return qlen

    def _GetFWHMAndPeaks(self):
        fwhm = np.zeros(len(self.parameters["RELEVANT_SCANS"]))
        peaks = np.zeros(len(self.parameters["RELEVANT_SCANS"]))
        for index in range(len(self.parameters["RELEVANT_SCANS"])):
            fwhm[index] = self._FindFWHM(self.fit_params[index])
            peaks[index] = max(self.fit[index])
        return fwhm, peaks

    def _GetIntegrals(self):
        integrals = np.zeros(len(self.parameters["RELEVANT_SCANS"]))
        for index in range(len(self.parameters["RELEVANT_SCANS"])):
            integrals[index] = self._Integrate(self.fit[index], self.q[index])
        return integrals

    def _ReadSeriesData(self):
        offset = self._GetDetectorOffset()
        data_length = self._DataLength()
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        #q = np.zeros((scan_length, data_length))
        #th = np.zeros((scan_length, data_length))
        #tth = np.zeros((scan_length, data_length))
        #sensor = np.zeros((scan_length, data_length))
        q = [0]*scan_length
        th = [0]*scan_length
        tth = [0]*scan_length
        sensor = [0]*scan_length
        index = 0
        for scan in self.parameters["RELEVANT_SCANS"]:
            th[index] = self.sData[scan-1]["Theta"]
            tth[index] = self.sData[scan-1]["TwoTheta"] + offset
            sensor[index] = self.sData[scan-1][self.parameters["SENSOR"]] / self.sData[scan-1]["I0_BD3"]
            index += 1
        if "TTH_ZONE" in self.parameters:
            th, tth, sensor, q, ymax = self._SliceTthZone(th, tth, sensor)
        else:
            ymax = max(sensor[0])
            for index in range(scan_length):
                q[index] = self.TwoThetaToQ(tth[index], self.energy[index])
                sensor[index] = self._RemoveOutliers(sensor[index])
                sensor[index] = self._Normalize(sensor[index], ymax)
        return th, tth, sensor, q, ymax

    def _SliceTthZone(self, th, tth, sensor):
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        idx1 = (np.abs(np.asarray(tth[0]) - self.parameters["TTH_ZONE"][0])).argmin()
        idx2 = (np.abs(np.asarray(tth[0]) - self.parameters["TTH_ZONE"][1])).argmin()
        q_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        th_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        tth_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        sensor_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        for index in range(scan_length):
            th_new[index] = th[index][idx1:idx2]
            tth_new[index] = tth[index][idx1:idx2]
            sensor_new[index] = sensor[index][idx1:idx2]
        ymax_new = max(sensor_new[0])
        for index in range(scan_length):
            q_new[index] = self.TwoThetaToQ(tth_new[index], self.energy[index])
            sensor_new[index] = self._RemoveOutliers(sensor_new[index])
            sensor_new[index] = self._Normalize(sensor_new[index], ymax_new)
        return th_new, tth_new, sensor_new, q_new, ymax_new

    def _FitSeriesData(self):
        data_length = len(self.sensor[0])
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        q = [0]*scan_length
        fit = [0]*scan_length
        chisqr = ['']*scan_length
        fit_params = ['']*scan_length
        for index in range(scan_length):
            fit[index], fit_params[index], chisqr[index] = self._CalculateFit(self.sensor[index], self.q[index])
        zero_indices = self._ZeroScans(self.sensor, self.q)
        try: #delete zero scans
            self._th = self.th[:zero_indices[0]]
            self._tth = self.tth[:zero_indices[0]]
            self._q = self.q[:zero_indices[0]]
            self._sensor = self.sensor[:zero_indices[0]]
            fit = fit[:zero_indices[0]]
            fit_params = fit_params[:zero_indices[0]]
            chisqr = chisqr[:zero_indices[0]]
            self._temp = self.temp[:zero_indices[0]]
            self._energy = self.energy[:zero_indices[0]]
            self._parameters["RELEVANT_SCANS"] = self.parameters["RELEVANT_SCANS"][:zero_indices[0]]
        except IndexError:
            pass
        return fit, fit_params, chisqr
   
    def _ZeroScans(self, y, x):
        minchisqr = 0.006 #magic number
        #minchisqr = 0.0002 #magic number
        zero_indices = []
        for index in range(len(self.parameters["RELEVANT_SCANS"])):
            mod = LinearModel()
            #mod = ConstantModel()
            pars = mod.guess(y[index], x=x[index])
            out = mod.fit(y[index], pars, x=x[index])
            if out.chisqr < minchisqr:
                zero_indices.append(index)
        return zero_indices[1:]

    def _CalculateFit(self, y, x, model="voigt"):
        mod = {"voigt": VoigtModel(), "gaussian": GaussianModel()}[model]
        pars = mod.guess(y, x=x)
        pars['gamma'].set(vary=True)
        #####################################
        pars['center'].set(min=0, max=100)
        pars['amplitude'].set(min=0, max=100)
        pars['sigma'].set(min=0, max=200)
        pars['gamma'].set(min=0, max=200)
        #####################################
        out = mod.fit(y, pars, x=x)
        return out.best_fit, out.params, out.chisqr

    def _ReadTempData(self):
        temp = []
        for scan in self.parameters["RELEVANT_SCANS"]:
            temp.append(np.mean(self.sData[scan-1]["Sample_T"]))
        return self._ListStringToFloat(temp)

    def _ReadEnergyData(self):
        energy = []
        for scan in self.parameters["RELEVANT_SCANS"]:
            energy.append(self.sInfo[scan-1]["MonoEngy"])
        return self._ListStringToFloat(energy)

    def _Integrate(self, y, x):
        return integrate.simps(y, x, even='avg')

    def _Normalize(self, sensor, ymax):
        sensor = sensor/ymax
        background = np.mean((sensor[0], sensor[1], sensor[2], sensor[3], sensor[4], sensor[-5], sensor[-4], sensor[-3], sensor[-2], sensor[-1]))
        return sensor-background

    def _RemoveOutliers(self, data):
        std = np.std(data)
        mean = np.mean(data)
        STD_CUTOFF = 4 #magic number
        outliers = []
        for index in range(len(data)):#find points that are greater than STD_CUTOFF standard deviations from the mean
            if abs(data[index] - mean)/std > STD_CUTOFF:
                outliers.append(index)
        for index in range(len(outliers)):#replace outliers with the average of the points on either side of the outlier
            data[outliers[index]] = np.mean([data[outliers[index]-1], data[outliers[index]+1]]) 
        return data

    def _FindFWHM(self, params):
        return params['fwhm'].value

    def _FindLocationOfPeak(self, params):
        return params['center'].value

########################################################################################################################################## 
class EnergyDataProcessing(DataProcessing):
    def __init__(self, parampath, fixedq=False):
        super().__init__(parampath)
        self._fixedq = fixedq
        self._energy = self._ReadEnergyData()
        self._sensor = self._ReadSensorData()
        self._th, self._tth = self._ReadThetaTwoThetaData()
        self._temp = self._ReadTempData()

    @property
    def fixedq(self):
        return self._fixedq

    @property
    def energy(self):
        return self._energy

    @property
    def sensor(self):
        return self._sensor

    @property
    def th(self):
        return self._th

    @property
    def tth(self):
        return self._tth

    @property
    def temp(self):
        return self._temp

#Public Methods
########################################################################
    def SavePlot(self, datatype="Fluorescence", title=None, fileName=None, aspectratio='auto', index=0):
        ax = self._PlotInitialize()
        if title == None:
            title = self.title
        if fileName == None:
            fileName = title + "_" + datatype
        ax.set_title(title)
        self._Plot(ax, datatype, aspectratio, index)
        plt.savefig(fileName)

    def ShowPlot(self, datatype="Fluorescence", title=None, aspectratio='auto', index=0):
        ax = self._PlotInitialize()
        if title == None:
            title = self.title
        ax.set_title(title)
        self._Plot(ax, datatype, aspectratio, index)
        plt.show()

    def SaveData(self, datatype="Fluorescence", filename=None):
        if filename == None:
            filename = self.title + "_" + datatype + ".txt"
        try:
            x, y = {"Fluorescence": (self.energy, self.sensor)}[datatype]
            np.savetxt(filename, np.transpose(np.vstack((x, y))))
        except KeyError: 
            print("ERROR in SaveData(): Data type '"+datatype+"' is not a valid option.")

#Private Methods
#####################################################################
    def _Plot(self, ax, datatype, aspectratio, index):
        if datatype == "Fluorescence":
            xlabel, ylabel = 'Energy (eV)', 'X-ray Fluorescence (arb. units)'
            ax.plot(self.energy[index], self.sensor[index]/max(self.sensor[index]), marker="o", label="Tth: "+str(self.sInfo[self.parameters["RELEVANT_SCANS"][0]-1]["TwoTheta"]))
            ax.set(xlim=(min(self.energy[index]), max(self.energy[index])))
        else:#reserved for future options 
            pass
        ###################
        ax.set_aspect(aspectratio)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.legend()

    def _ReadEnergyData(self):
        data_length = self._DataLength()
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        energy = np.zeros((scan_length, data_length))
        index = 0
        for scan in self.parameters["RELEVANT_SCANS"]:
            energy[index] = self.sData[scan-1]["MonoEngy"]
            index += 1
        return energy

    def _ReadSensorData(self):
        data_length = self._DataLength()
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        sensor = np.zeros((scan_length, data_length))
        index = 0
        for scan in self.parameters["RELEVANT_SCANS"]:
            sensor[index] = (self.sData[scan-1][self.parameters["SENSOR"]] / self.sData[scan-1]["I0_BD3"])
            index += 1
        return sensor

    def _ReadThetaTwoThetaData(self):
        offset = self._GetDetectorOffset()
        data_length = self._DataLength()
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        th = np.zeros((scan_length, data_length))
        tth = np.zeros((scan_length, data_length))
        index = 0
        if self.fixedq == True:
            for scan in self.parameters["RELEVANT_SCANS"]:
                th[index] = self.sData[scan-1]["Theta"]
                tth[index] = self.sData[scan-1]["TwoTheta"] + offset
                index += 1
        else:
            for scan in self.parameters["RELEVANT_SCANS"]:
                th[index] = self.sInfo[scan-1]["Theta"]
                tth[index] = self.sInfo[scan-1]["TwoTheta"] + offset
                index += 1
        return th, tth

    def _ReadTempData(self):
        temp = []
        for scan in self.parameters["RELEVANT_SCANS"]:
            temp.append(np.mean(self.sData[scan-1]["Sample_T"]))
        return self._ListStringToFloat(temp)
###########################################################################################################################################
class ThTthMeshDataProcessing(DataProcessing):
    def __init__(self, parampath, fit=True):
        # only temp series plotting working is raw data in flipshow format
        super().__init__(parampath)
        #self._energy = self._ReadEnergyData()
        #self._temp = self._ReadTempData()
        self._th, self._tth, self._sensor, self._q, self._ymax, self._energy, self._temp = self._ReadSeriesData() 
        self._yspace = 1/10 #magic number for series plotting spacing
        self._qlen = self._GetQLen()
        #if fit == True:
        #    self._fit, self._fit_params, self._chisqr = self._FitSeriesData()
        #    self._integrals = self._GetIntegrals()
    
    @property
    def energy(self):
        return self._energy

    @property
    def ymax(self):
        return self._ymax

    @property
    def temp(self):
        return self._temp

    @property
    def yspace(self):
        return self._yspace

    @yspace.setter
    def yspace(self, yspace):
        print("Setting 'yspace' to ", yspace)
        self._yspace = yspace

    @property
    def th(self):
        return self._th

    @property
    def tth(self):
        return self._tth

    @property
    def sensor(self):
        return self._sensor

    @property
    def q(self):
        return self._q

    @property
    def fit(self):
        return self._fit

    @property
    def fit_params(self):
        return self._fit_params

    @property
    def chi_sqr(self):
        return self._chisqr

    @property
    def qlen(self):
        return self._qlen

    @property
    def integrals(self):
        return self._integrals

# Public methods
#############################################################
    def SavePlot(self, datatype, title=None, fileName=None, aspectratio='auto', index=0):
        ax = self._PlotInitialize()
        if title == None:
            title = self.title
        if fileName == None:
            fileName = title + "_" + datatype
        ax.set_title(title)
        self._Plot(ax, datatype, aspectratio, index)
        plt.savefig(fileName)

    def ShowPlot(self, datatype, title=None, aspectratio='auto', index=0):
        if datatype == "TempSeries_Raw":
            ax = self._Plot3DInitialize()
        else:
            ax = self._PlotInitialize()
        if title == None:
            title = self.title
        ax.set_title(title)
        self._Plot(ax, datatype, aspectratio, index)
        plt.show()

    def SaveData(self, datatype, filename=None):
        if filename == None:
            filename = self.title + "_" + datatype + ".txt"
        if datatype == "Integrals":#Integrals is special because a zero division is possible without this if statement
            x, y = np.mean(self.temp), self.integrals/(max(self.integrals))
            np.savetxt(filename, np.transpose(np.vstack((x, y))))
        else:
            try:
                x, y = {"QLen": (np.mean(self.temp), self.qlen), "TempSeries_Raw_Tth": (self.tth, self.sensor), "TempSeries_Raw": (self.q, self.sensor), "TempSeries_Fit": (self.q, self.fit)}[datatype]
                if datatype=="TempSeries_Raw" or datatype=="TempSeries_Fit" or datatype=="TempSeries_Raw_Tth":#slightly more complicated to store series data
                    with open(filename, 'w') as f:
                        for index in range(len(x)):
                            values = np.transpose(np.vstack((x[index], y[index])))
                            string = ""
                            for index1 in range(len(values)):
                                string = string + "{:e}".format(values[index1][0]) + " " + "{:e}".format(values[index1][1]) + "\n"
                            f.write("%s,\n" % string)
                else:
                    np.savetxt(filename, np.transpose(np.vstack((x, y))))
            except KeyError: 
                if datatype=="TempSeries_Both":
                    print("ERROR in SaveData(): You cannot save both the Raw data and the Fit data to the same file. Try processing and saving the Raw and Fit data separately if you want to save the data.")
                else:
                    print("ERROR in SaveData(): Data type '"+datatype+"' is not a valid option.")

    def PrintsData(self):
        print("\n\nFields available in sData:")
        print(self.parameters["RELEVANT_SCANS"][0][0]-1)
        for field in self.sData[self.parameters["RELEVANT_SCANS"][0][0]-1]: print(field)

    def PrintsInfo(self):
        print("\n\nFields available in sInfo:")
        for field in self.sInfo[self.parameters["RELEVANT_SCANS"][0][0]-1]: print(field)

# Private method
##############################################################
    def _Plot(self, ax, datatype, aspectratio, index):
        if datatype == "QLen":
            xlabel, ylabel = 'Temperature (K)', 'Peak Location (q)'
            #ax.plot(self.qlen, self._temp, color='r')
            #ax.invert_yaxis()
            ax.plot(self.temp, self.qlen, color='r', marker="o")
            #ax.set(ylim=(min(self._temp), max(self._temp)))
            #ax.set(xlim=(min(self.qlen), max(self.qlen)))
            offset = (max(self.qlen) - min(self.qlen))/15 #magic offset
            ax.set(xlim=(min(self.temp), max(self.temp)))
            ax.set(ylim=(min(self.qlen), max(self.qlen)+offset))

        elif datatype == "Integrals":
            xlabel, ylabel = 'Temperature (K)', 'Integrated Intensity (arb. units)'
            ax.plot(self._temp, self.integrals/max(self.integrals), marker="o")
            ax.set(xlim=(min(self.temp), max(self.temp)))
        
        else: #reserved for temperature series options
            xlabel, ylabel = r'|q|(${\AA}^{-1}$)', 'Intensity (arb. units)'
            if datatype == "TempSeries_Raw":
                xlabel, ylabel = 'th', 'tth'
                while True:
                    for index in range(len(self.parameters["RELEVANT_SCANS"])):
                        ax = plt.axes(projection='3d')
                        #ax.view_init(0, -90) #view from th
                        ax.view_init(0, 180) #view from tth
                        #ax.xaxis.set_ticks(np.arange(param.TH0_START, param.TH2_END+1, 0.5))
                        ax.set_xlabel('th')
                        ax.set_ylabel('tth')
                        ax.set_zlabel('pd')
                        norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
                        #ax.set_zlim([-0.5*10**(-15),5*10**(-15)])
                        ax.set_zlim([0, 1])
                        ax.set_title(str(self.temp[index]))
                        cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
                        offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
                        #ax.plot(self.q[index], self.sensor[index]+offset, c=cmap.to_rgba(index + 1))
                        ax.scatter(np.asarray(self.th[index]).astype(np.float), np.asarray(self.tth[index]).astype(np.float), np.asarray(self.sensor[index]).astype(np.float), c=np.asarray(self.sensor[index]).astype(np.float), cmap='cividis_r')
                        plt.pause(0.1)
                #ax.set(xlim=(min(self.q[0]), max(self.q[0])))
            #elif datatype == "TempSeries_Fit":
            #    for index in range(len(self.parameters["RELEVANT_SCANS"])):
            #        norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
            #        cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
            #        offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
            #        ax.plot(self.q[index], self.fit[index]+offset, c=cmap.to_rgba(index + 1))
            #    ax.set(xlim=(min(self.q[0]), max(self.q[0])))
            #elif datatype == "TempSeries_Raw_Tth":
            #    for index in range(len(self.parameters["RELEVANT_SCANS"])):
            #        norm = mpl.colors.Normalize(vmin=1, vmax=len(self.parameters["RELEVANT_SCANS"])+1)
            #        cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
            #        offset = (len(self.parameters["RELEVANT_SCANS"])-index-1)*self.yspace
            #        ax.plot(self.tth[index], self.sensor[index]+offset, c=cmap.to_rgba(index + 1))
            #    ax.set(xlim=(min(self.tth[0]), max(self.tth[0])))
            else:
                print("ERROR: Data type '"+datatype+"' is not a valid option.")
            if aspectratio != 'equal' and aspectratio != 'auto':
                ax.set(ylim=(0, 1+self.yspace*len(self.parameters["RELEVANT_SCANS"])-(aspectratio/self.yspace)))
            else:
                #ax.set(ylim=(0, 1+self.yspace*len(self.parameters["RELEVANT_SCANS"])))
                pass
            norm = mpl.colors.Normalize(vmin=min(self.temp), vmax=max(self.temp))
            cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
            plt.colorbar(cmap, ticks=np.rint(self._temp)).ax.invert_yaxis()
        ###################
        ax.set_aspect(aspectratio)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        #ax.legend()

    def _GetQLen(self):
        qlen = self._FindLocationOfPeak()
        return qlen

    def _GetIntegrals(self):
        integrals = np.zeros(len(self.parameters["RELEVANT_SCANS"]))
        for index in range(len(self.parameters["RELEVANT_SCANS"])):
            integrals[index] = self._Integrate(self.fit[index], self.q[index])
        return integrals

    def _ReadSeriesData(self):
        offset = self._GetDetectorOffset()
        #data_length = self._DataLength()
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        q = [0]*scan_length
        th = [0]*scan_length
        tth = [0]*scan_length
        sensor = [0]*scan_length
        temp = [0]*scan_length
        energy = [0]*scan_length
        scans = self.parameters["RELEVANT_SCANS"]

        repeats1 = []
        for index in range(self.parameters["TTH1_POINTS"]):
            repeats1.append(index*self.parameters["TH1_POINTS"])
        repeats2 = []
        for index in range(self.parameters["TTH2_POINTS"]):
            repeats2.append(index*self.parameters["TH2_POINTS"])

        #Note the spec scan numbers start at 1, but in sInfo and sData they start
        #at zero, so we need to account for this when accessing
        
        #loop through the meshes, merge them together, and save to filename identified by average temperature
        #also save total integrated intensity for each temperature
        index = 0
        for i in range(len(scans)):
            for j in range(len(scans[i])):
                scan = scans[i][j]
                if(j == 0):
                    sensor[index] = (self.sData[scan-1][self.parameters["SENSOR"]] / self.sData[scan-1]["I0_BD3"])
                    th[index] = self.sData[scan-1]["Theta"]
                    tth[index] = self.sData[scan-1]["TwoTheta"] + offset
                    temp[index] = self.sData[scan-1]["Sample_T"]
                    energy[index] = self.sInfo[scan-1]["MonoEngy"]
                elif(j == 1):
                    #repeat values are deleted
                    sensor[index] = np.hstack((sensor[index], np.delete(self.sData[scan-1][self.parameters["SENSOR"]] / self.sData[scan-1]["I0_BD3"], repeats1)))
                    th[index] = np.hstack((th[index], np.delete(self.sData[scan-1]["Theta"], repeats1)))
                    tth[index] = np.hstack((tth[index], np.delete(self.sData[scan-1]["TwoTheta"], repeats1))) + offset
                    temp[index] = np.hstack((temp[index], np.delete(self.sData[scan-1]["Sample_T"], repeats1)))
                    energy[index] = np.hstack((energy[index], self.sInfo[scan-1]["MonoEngy"]))
                elif(j == 2):
                    #repeat values are deleted
                    sensor[index] = np.hstack((sensor[index], np.delete(self.sData[scan-1][self.parameters["SENSOR"]] / self.sData[scan-1]["I0_BD3"], repeats2)))
                    th[index] = np.hstack((th[index], np.delete(self.sData[scan-1]["Theta"], repeats2)))
                    tth[index] = np.hstack((tth[index], np.delete(self.sData[scan-1]["TwoTheta"], repeats2))) + offset
                    temp[index] = np.hstack((temp[index], np.delete(self.sData[scan-1]["Sample_T"], repeats2)))
                    energy[index] = np.hstack((energy[index], self.sInfo[scan-1]["MonoEngy"]))
            energy[index] = np.mean(energy[index])
            temp[index] = np.mean(temp[index])
            index += 1
        #if "TTH_ZONE" in self.parameters:
        #    th, tth, sensor, q, ymax = self._SliceTthZone(th, tth, sensor)
        #else:
        #    ymax = max(sensor[0])
        #    for index in range(scan_length):
        #        q[index] = self.TwoThetaToQ(tth[index], self.energy[index])
        #        sensor[index] = self._RemoveOutliers(sensor[index])
        #        sensor[index] = self._Normalize(sensor[index], ymax)
        ymax = max(sensor[0])
        for index in range(scan_length):
            q[index] = self.TwoThetaToQ(tth[index], energy[index])
            sensor[index] = self._RemoveOutliers(sensor[index])
            sensor[index] = self._Normalize(sensor[index], ymax)
        return th, tth, sensor, q, ymax, energy, temp

    def _Normalize(self, sensor, ymax):
        sensor = sensor/ymax
        background = np.mean((sensor[0], sensor[1], sensor[2], sensor[3], sensor[4], sensor[-5], sensor[-4], sensor[-3], sensor[-2], sensor[-1]))
        return sensor-background

    def _Plot3DInitialize(self):
        self._ClearPlot()
        return plt.axes(projection='3d')

###########
    def _FitSeriesData(self):
        data_length = len(self.sensor[0])
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        q = np.zeros((scan_length, data_length))
        fit = np.zeros((scan_length, data_length))
        chisqr = ['']*scan_length
        fit_params = ['']*scan_length
        for index in range(scan_length):
            fit[index], fit_params[index], chisqr[index] = self._CalculateFit(self.sensor[index], self.q[index])
        zero_indices = self._ZeroScans(self.sensor, self.q)
        try: #delete zero scans
            self._th = self.th[:zero_indices[0]]
            self._tth = self.tth[:zero_indices[0]]
            self._q = self.q[:zero_indices[0]]
            self._sensor = self.sensor[:zero_indices[0]]
            fit = fit[:zero_indices[0]]
            fit_params = fit_params[:zero_indices[0]]
            chisqr = chisqr[:zero_indices[0]]
            self._temp = self.temp[:zero_indices[0]]
            self._energy = self.energy[:zero_indices[0]]
            self._parameters["RELEVANT_SCANS"] = self.parameters["RELEVANT_SCANS"][:zero_indices[0]]
        except IndexError:
            pass
        return fit, fit_params, chisqr
   
    def _ZeroScans(self, y, x):
        minchisqr = 0.006 #magic number
        zero_indices = []
        for index in range(len(self.parameters["RELEVANT_SCANS"])):
            mod = LinearModel()
            #mod = ConstantModel()
            pars = mod.guess(y[index], x=x[index])
            out = mod.fit(y[index], pars, x=x[index])
            if out.chisqr < minchisqr:
                zero_indices.append(index)
        return zero_indices[1:]

    def _CalculateFit(self, y, x, model="voigt"):
        mod = {"voigt": VoigtModel(), "gaussian": GaussianModel()}[model]
        pars = mod.guess(y, x=x)
        pars['gamma'].set(vary=True)
        #####################################
        pars['center'].set(min=0, max=100)
        pars['amplitude'].set(min=0, max=100)
        pars['sigma'].set(min=0, max=200)
        pars['gamma'].set(min=0, max=200)
        #####################################
        out = mod.fit(y, pars, x=x)
        return out.best_fit, out.params, out.chisqr

    def _ReadTempData(self):
        temp = []
        for scan in self.parameters["RELEVANT_SCANS"]:
            print(self.sData[scan[0]-1]["Sample_T"])
            temp.append(np.mean(self.sData[scan-1]["Sample_T"]))
        return self._ListStringToFloat(temp)

    def _ReadEnergyData(self):
        energy = []
        for scan in self.parameters["RELEVANT_SCANS"]:
            energy.append(self.sInfo[scan-1]["MonoEngy"])
        return self._ListStringToFloat(energy)

    def _Integrate(self, y, x):
        return integrate.simps(y, x, even='avg')

    def _RemoveOutliers(self, data):
        #print(data)
        std = np.std(data)
        mean = np.mean(data)
        STD_CUTOFF = 4 #magic number
        outliers = []
        for index in range(len(data)):#find points that are greater than STD_CUTOFF standard deviations from the mean
            if abs(data[index] - mean)/std > STD_CUTOFF:
                outliers.append(index)
        for index in range(len(outliers)):#replace outliers with the average of the points on either side of the outlier
            data[outliers[index]] = np.mean([data[outliers[index]-1], data[outliers[index]+1]]) 
        return data

    def _FindLocationOfPeak(self):
        qlen = []
        for index in range(len(self.sensor)):
            idx = self.sensor[index].argmax()
            qlen.append(self.TwoThetaToQ(self.tth[index][idx], self.energy[index]))
        return qlen

    def _DataLength(self):
        index = self.parameters["RELEVANT_SCANS"][0]
        length = len(self.sData[index-1]["Sample_T"])
        return self.datalength
         
    def _SliceTthZone(self, th, tth, sensor):
        scan_length = len(self.parameters["RELEVANT_SCANS"])
        idx1 = (np.abs(np.asarray(tth[0]) - self.parameters["TTH_ZONE"][0])).argmin()
        idx2 = (np.abs(np.asarray(tth[0]) - self.parameters["TTH_ZONE"][1])).argmin()
        q_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        th_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        tth_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        sensor_new = np.zeros((scan_length, len(tth[0][idx1:idx2])))
        for index in range(scan_length):
            th_new[index] = th[index][idx1:idx2]
            tth_new[index] = tth[index][idx1:idx2]
            sensor_new[index] = sensor[index][idx1:idx2]
        ymax_new = max(sensor_new[0])
        for index in range(scan_length):
            q_new[index] = self.TwoThetaToQ(tth_new[index], self.energy[index])
            sensor_new[index] = self._RemoveOutliers(sensor_new[index])
            sensor_new[index] = self._Normalize(sensor_new[index], ymax_new)
        return th_new, tth_new, sensor_new, q_new, ymax_new

#if __name__ == "__main__":
    #test
    

