#!/usr/local/bin/python3
import csv
import numpy as np
import sys
from SFO_STO_Parameters import *
from scipy.interpolate import griddata

def InterpolateMeshScans(th, tth, pd):
  TH, TTH = np.meshgrid(np.linspace(TH0_START, TH2_END, int((TH2_END-TH0_START)/DESIRED_RES+1)), np.linspace(TTH0_START, TTH2_END, int((TTH2_END-TTH0_START)/DESIRED_RES+1)))
  pdInter = griddata((th, tth), pd, (TH, TTH), method='cubic')
  return TH.flatten(), TTH.flatten(), pdInter.flatten()
  #return TH, TTH, pdInter

for index in range(len(sys.argv)-1):
  th = []
  tth = []
  pd = []
  reader = csv.reader(open(sys.argv[index+1]), delimiter=" ")
  for row in reader:
    th.append(row[0])
    tth.append(row[1])
    pd.append(row[2])
  th = np.asarray(th).astype(np.float)
  tth = np.asarray(tth).astype(np.float)
  pd = np.asarray(pd).astype(np.float)
  TH, TTH, PDInter = InterpolateMeshScans(th, tth, pd)
  np.savetxt(sys.argv[index+1].replace('.dat', '_Interpolated.dat'),np.transpose(np.vstack((TH,TTH,PDInter))))
