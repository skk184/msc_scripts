#!/usr/local/bin/python3
import SFO_DataAnalysis as da
import matplotlib.pyplot as plt

files = [
#'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet/',
#'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_ZONE1/',
##'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_ZONE2/',
#'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP/'#,
#'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP_ZONE1/'#,
#'../processed_data/2020_08_26_REIXS_SFO-LSAT-T-Magnet_MCP_ZONE2/'#,
'../processed_data/2019_12_11_REIXS_SFO-LAO/',
'../processed_data/2020_01_14_REIXS_SFO-LSAT-B-REMNT/',
'../processed_data/2020_01_14_REIXS_SFO-STO-B-RECTR/',
'../processed_data/2020_01_14_REIXS_SFO-LSAT-T-REMNT/'#,
##'../processed_data/2020_03_04_REIXS_SFO-LSAT-B-Magnet/' 
]

datatypes = ["QLen", "FWHM", "Integrals", "TempSeries_Raw_Tth", "TempSeries_Raw", "TempSeries_Fit", "TempSeries_Both"]
#datatypes = ["QLen", "FWHM", "Integrals"]
#datatypes = ["TempSeries_Raw_Tth", "TempSeries_Raw", "TempSeries_Fit", "TempSeries_Both"]
#datatypes = ["TempSeries_Raw", "TempSeries_Fit", "TempSeries_Both"]
#datatypes = ["TempSeries_Raw_Tth"]
datatypes = ["QLen"]

def all_data(files, datatypes):
    #for index in range(len([0])):
    ax = plt.axes()
    for index in range(len(files)):
        data = da.ThTthDataProcessing(files[index], fit=True)
        ax.plot(data.temp, data.qlen, marker="o", label=data.title)
        #data.yspace = 1/10
        for index2 in range(len(datatypes)):
            #data.ShowPlot(datatypes[index2])
            #data.ShowPlot(datatypes[index2], aspectratio=0.03)
            #data.SavePlot(datatypes[index2])
            #data.SavePlot(datatypes[index2], aspectratio=0.03)
            #data.SaveData(datatypes[index2])
            pass
    ax.set_xlabel("Temperature (K)") 
    ax.set_ylabel("Peak Location (q)")
    plt.legend()
    plt.savefig("SFO-LAO_STO_LSAT-QLen_All.png")

all_data(files, datatypes)
