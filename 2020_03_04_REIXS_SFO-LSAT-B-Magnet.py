{
"TYPE": "not_mesh",
"SENSOR": "SDD_TOT",
"DATA_FILE": "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_03_04_REIXS/Data/SFO-LSAT-B-Magnet.spec",
"OUTPUT_PREFIX": "2020_03_04_REIXS_SFO-LSAT-B-Magnet_",
"OUTPUT_LOCATION": "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2020_03_04_REIXS_SFO-LSAT-B-Magnet/",
"RELEVANT_SCANS": [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37],
"MULTIPLE_ZONES": "False",
"OFFSET_OVERRIDE": "False"
}
