#!/usr/local/bin/python3
import csv
import numpy as np
import sys
from SFO_STO_Parameters import *


maximums=[]
temperature=[]
for index in range(len(sys.argv)-1):
  th = []
  tth = []
  pd = []
  reader = csv.reader(open(sys.argv[index+1]), delimiter=" ")
  for row in reader:
    th.append(row[0])
    tth.append(row[1])
    pd.append(row[2])
  th = np.asarray(th).astype(np.float)
  tth = np.asarray(tth).astype(np.float)
  pd = np.asarray(pd).astype(np.float)
  temperature.append(float(sys.argv[index+1].partition('K')[0].replace('SFO-STO_', '')))
  maximums.append(np.max(pd))
print(temperature)
print(maximums)
temperature, maximums = zip(*sorted(zip(temperature, maximums)))
print(temperature)
print(maximums)
np.savetxt("SFO-STO_Main_Peak.dat",np.transpose(np.vstack((temperature,maximums))))
