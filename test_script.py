#!/usr/local/bin/python3
import helperFunctions as hf
import numpy as np
from decimal import Decimal

def Formatting(value, digits):
    #digits is the amount of sigfigs eg. digits=2 -> 1.2eX
    concat = '{0:.'+ str(digits-1) +'e}'
    try:
        return float(concat.format(Decimal(value)))
    except TypeError:
        return value

testing = hf.DataProcessing()
errors = 0 
##################################################################
# Formatting(value)
tests = [
    {'input': 1e6,
    'input2': 3,
    'output': 1e6, 
    'reason': "Input is positive and has less sigfigs than desired precision"},
    {'input': 1.234e5,
    'input2': 2,
    'output': 1.2e5, 
    'reason': "Input is positive and has more sigfigs than desired precision"},
    {'input': 0,
    'input2': 1,
    'output': 0, 
    'reason': "Zero"},
    {'input': -1234,
    'input2': 6,
    'output': -1234, 
    'reason': "Input is negative and has less sigfigs than desired precision"},
    {'input': -1234,
    'input2': 1,
    'output': -1000, 
    'reason': "Input is negative and has more sigfigs than desired precision"},
    {'input': 0.12345,
    'input2': 3,
    'output': 0.123, 
    'reason': "Leading 0, input has more sigfigs than desired precision"},
    {'input': 1.2345,
    'input2': 3,
    'output': 1.23, 
    'reason': "sdfadfa"},
    {'input': 1.234e-6,
    'input2': 3,
    'output': 1.23e-6, 
    'reason': "Very small number less than 1"},
    {'input': 0.12340000,
    'input2': 6,
    'output': 0.123400, 
    'reason': "Trailing zeros"}
]

for t in tests:
    inputs = t['input']
    inputs2 = t['input2']
    expected = t['output']
    message = t['reason']
    output = Formatting(inputs, inputs2)
    if output != expected:
        errors += 1
        print("input: ", inputs)
        print("input2: ", inputs2)
        print("output: ", output)
        print("expected: ", expected)
        print(message)

##################################################################
# ElectronVoltToJoule(energy) test
tests = [
    {'input': 0,
    'output': 0,
    'reason' : 'zero'},
    {'input': 709,
    'output': 1.135943234e-16,
    'reason': 'typical value'},
    {'input': 709.1,
    'output': 1.136103451e-16,
    'reason': 'added precision'},
    {'input': 709.11,
    'output': 1.136119473e-16,
    'reason': 'added precision'}
]

for t in tests:
    inputs = t['input']
    expected = t['output']
    message = t['reason']
    output = Formatting(testing.ElectronVoltToJoule(inputs), 10) 
    if output != expected:
        errors += 1
        print("input: ", inputs)
        print("output: ", output)
        print("expected: ", expected)
        print(message)

##################################################################
# EnergyToWavelength(energy)
tests = [
    {'input': 1e-309,
    'output': float('inf'),
    'reason': 'value that causes division by zero error'},
    {'input': 709,
    'output': 1.7487e-09,#https://www2.chemistry.msu.edu/faculty/reusch/virttxtjml/cnvcalc.htm 
    'reason': 'typical value'},
    {'input': 709.11,
    'output': 1.7484e-09, 
    'reason': 'added precision'},
    {'input': 1e-20,
    'output': 1.2398e+14, 
    'reason': 'added precision'}
] 

for t in tests:
    inputs = t['input']
    expected = t['output']
    message = t['reason']
    output = Formatting(testing.EnergyToWavelength(inputs), 5)
    if output != expected:
        errors += 1
        print("input: ", inputs)
        print("output: ", output)
        print("expected: ", expected)
        print(message)

##################################################################
# TwoThetaToQ(TTH, energy) test. checked against https://www.ill.eu/fileadmin/user_upload/ILL/3_Users/Support_labs_infrastructure/Software-tools/DIF_tools/neutrons.html
tests = [
    {'input': 56,
    'input2': 709,#1.7487193007623054e-09
    'output': 0.337364e10, 
    'reason': "Common values"},
    {'input': 0,
    'input2': 709,
    'output': 0, 
    'reason': "Two theta is 0"},
    {'input': 180,
    'input2': 709,
    'output': 0.718604e10, 
    'reason': "Two theta is 180"},
    {'input': np.asarray([56, 0, 180]),
    'input2': 709,
    'output': np.asarray([0.337364e10, 0, 0.718604e10]), 
    'reason': "Two theta is 180"}
]

for t in tests:
    inputs = t['input']
    inputs2 = t['input2']
    expected = t['output']
    message = t['reason']
    output = Formatting(testing.TwoThetaToQ(inputs, inputs2), 6)
    try:
        if output.all() != expected.all():
            errors += 1
            print("input: ", inputs)
            print("input2: ", inputs2)
            print("output: ", output)
            print("expected: ", expected)
            print(message)
    except AttributeError:
        if output != expected:
            errors += 1
            print("input: ", inputs)
            print("input2: ", inputs2)
            print("output: ", output)
            print("expected: ", expected)
            print(message)
        
##################################################################
# Integrate(y, x) test. Compared to: https://www.wolframalpha.com/widgets/gallery/view.jsp?id=174a81e7a9ffb5aed0a790093981aaab
x1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] #n=10
x2 = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20] #n=10
x3 = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21] #n=11
x4 = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22] #n=12
x5 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] #n=10
x6 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] #n=9

def f1(x):
    return x**2

def f2(x):
    return np.log(x)

tests = [
    {'input': [f1(i) for i in x1],
    'input2': x1,
    'output': 333.333, 
    'reason': "x^2, n=even"},
    {'input': [f2(i) for i in x2],
    'input2': x2,
    'output': 26.8888, 
    'reason': "log(x), n=even"},
    {'input': [f1(i) for i in x2],
    'input2': x2,
    'output': 2333.33, 
    'reason': "x^2, n=even"},
    {'input': [f1(i) for i in x3],
    'input2': x3,
    'output': 2753.67, 
    'reason': "x^2, n=odd"},
    {'input': [f2(i) for i in x3],
    'input2': x3,
    'output': 29.9091, 
    'reason': "log(x), n=odd"},
    {'input': [f1(i) for i in x4],
    'input2': x4,
    'output': 3216, 
    'reason': "x^2, n=even"},
    {'input': [f2(i) for i in x4],
    'input2': x4,
    'output': 32.9771, 
    'reason': "log(x), n=even"},
    {'input': [f1(i) for i in x5],
    'input2': x5,
    'output': 443.333, 
    'reason': "x^2, n=even"},
    {'input': [f2(i) for i in x5],
    'input2': x5,
    'output': 16.3763, 
    'reason': "log(x), n=even"},
    {'input': [f1(i) for i in x6],
    'input2': x6,
    'output': 243, 
    'reason': "x^2, n=odd"}
]

for t in tests:
    inputs = t['input']
    inputs2 = t['input2']
    expected = t['output']
    message = t['reason']
    output = Formatting(testing.Integrate(inputs, inputs2), 6)
    if output != expected:
        errors += 1
        print("input: ", inputs)
        print("input2: ", inputs2)
        print("output: ", output)
        print("expected: ", expected)
        print(message)


print(errors, " errors")


#    {'input': ,
#    'output': , 
#    'reason': },


