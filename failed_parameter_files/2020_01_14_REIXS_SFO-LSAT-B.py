TYPE = "not_mesh"

SENSOR = "SDD_TOT"

DATA_FILE = "/Users/skylar/OneDrive - University of Saskatchewan/raw_data/2020_01_14_REIXS/Data/SFO-LSAT-B.spec"

OUTPUT_PREFIX = "SFO-LSAT-B_"

OUTPUT_LOCATION = "/Users/skylar/OneDrive - University of Saskatchewan/processed_data/2020_01_14_REIXS/SFO-LSAT-B/" 

RELEVANT_SCANS = [31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63]
