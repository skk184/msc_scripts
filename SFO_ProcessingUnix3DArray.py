#!/usr/local/bin/python3
#the line below should be the directory where SpecFileProcessing.py is stored
import sys
sys.path.insert(1, '~/OneDrive - University of Saskatchewan/scripts/')
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from SpecFileProcessingUnix import *
from scipy.interpolate import griddata

data_file = sys.argv[1]

def ProcessMeshScans(fname,scans):

  #Read the data and info from the spec file into lists
  sInfo,sData = ReadSpecFile(fname)
  
  
  #An example of the fields available for a particular scan:
  print("\n\nFields available in sInfo:")
  for field in sInfo[scans[0][0]-1]:
    print(field)
    
  print("\n\nFields available in sData:")
  for field in sData[scans[0][0]-1]:
    print(field)
  
  #the edges between two meshes were double counted
  #for now we will remove the second count but averaging them is also an option
  #values defined in "SFO_Script"
  TH0_START = 31
  TH0_END = 34
  TH0_POINTS = 15 + 1
  TTH0_START = 57
  TTH0_END = 65
  TTH0_POINTS = 40 + 1

  TH1_START = 34
  TH1_END = 35.6
  TH1_POINTS = 16 + 1
  TTH1_START = 57
  TTH1_END = 65
  TTH1_POINTS = 80 + 1

  TH2_START = 35.6
  TH2_END = 38
  TH2_POINTS = 12 + 1
  TTH2_START = 57
  TTH2_END = 65
  TTH2_POINTS = 40 + 1

  #find the indices of repeated values in each region
  repeats1 = []
  for index in range(TTH1_POINTS):
    repeats1.append(index*TH1_POINTS)
  repeats2 = []
  for index in range(TTH2_POINTS):
    repeats2.append(index*TH2_POINTS)
  
  #Note the spec scan numbers start at 1, but in sInfo and sData they start
  #at zero, so we need to account for this when accessing


  #loop through the meshes, merge them together, and save to filename identified by average temperature
  #also save total integrated intensity for each temperature
  temps = np.zeros((len(scans),2))
  for i in range(len(scans)):
    for j in range(len(scans[i])):
      scan = scans[i][j]
      if(j == 0):
        pd0 = sData[scan-1]["PicoAm2"] / sData[scan-1]["I0_BD3"]
      elif(j == 1):
        #repeat values are deleted
        pd1 = np.delete(sData[scan-1]["PicoAm2"] / sData[scan-1]["I0_BD3"], repeats1)
      elif(j == 2):
        pd2 = np.delete(sData[scan-1]["PicoAm2"] / sData[scan-1]["I0_BD3"], repeats2)
  
    #np.savetxt("SFO-STO-Testing_" + "K.dat",np.transpose(np.vstack((pd0,pd1,pd2))))
  #np.savetxt("SFO-STO-Testing_TDep.dat",temps)
  array0 = np.array([[0 for k in range(TH0_POINTS)] for j in range(TTH0_POINTS)], dtype=float)
  for i in range(TTH0_POINTS):
    thscan = []
    for j in range(TH0_POINTS):
      thscan.append(pd0[i*TH0_POINTS+j])
    array0[i] = thscan
      
  array1 = np.array([[0 for k in range(TH1_POINTS-1)] for j in range(TTH1_POINTS)], dtype=float)
  for i in range(TTH1_POINTS):
    thscan = []
    for j in range(TH1_POINTS-1):
      thscan.append(pd1[i*(TH1_POINTS-1)+j])
    array1[i] = thscan

  array2 = np.array([[0 for k in range(TH2_POINTS-1)] for j in range(TTH2_POINTS)], dtype=float)
  for i in range(TTH2_POINTS):
    thscan = []
    for j in range(TH2_POINTS-1):
      thscan.append(pd2[i*(TH2_POINTS-1)+j])
    array2[i] = thscan

  #print(array0.shape)
  #print(array1.shape)
  #print(array2.shape)
  
  #fig = plt.figure()
  #ax = plt.axes(projection='3d')
  tho = np.linspace(TH0_START, TH0_END, TH0_POINTS)
  ttho = np.linspace(TTH0_START, TTH0_END, TTH0_POINTS)
  TTHo, THo = np.meshgrid(ttho, tho)
  print("original th: ", tho.shape)
  print("pd1: ", tho.shape)

  print("original tth: ", ttho.shape)
  print("z: ", array0.T.flatten().shape)
  thn = np.linspace(TH0_START, TH0_END, TH0_POINTS*2)
  tthn = np.linspace(TTH0_START, TTH0_END, TTH0_POINTS*2)
  #grid_x, grid_y = np.mgrid[thn, tthn]
  #grid_x, grid_y = np.mgrid[TH0_START:TH0_END:TH0_POINTS*2j, TTH0_START:TTH0_END:TTH0_POINTS*2j]
  grid_x, grid_y = np.mgrid[TTH0_START:TTH0_END:TTH0_POINTS*2j, TH0_START:TH0_END:TH0_POINTS*2j]
  print("new th : ", grid_x.shape)
  print("new tth : ", grid_y.shape)
  pdInter = griddata((ttho, tho), array0.T.flatten(), (grid_x, grid_y), method='nearest')
  #ax.contour3D(TH, TTH, array0, 50, cmap='binary')
  #fig.savefig('demo.png', bbox_inches='tight')

#ProcessMeshScans("/Volumes/data/" + data_file,[[23,24,25]])
ProcessMeshScans("/Volumes/data/" + data_file,[[19],[20,21,22],[23,24,25],[26,27,28],[29,30,31],[32,33,34],[35,36,37],[38,39,40],[41,42,43],[44,45,46],[47,48,49],[50,51,52],[53,54,55]])



