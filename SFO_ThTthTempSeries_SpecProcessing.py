#!/usr/local/bin/python3
#the line below should be the directory where SpecFileProcessing.py is stored
import sys
sys.path.insert(1, '~/OneDrive - University of Saskatchewan/msc_scripts/')
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from SpecFileProcessingUnix import *
from scipy.interpolate import griddata
import tkinter
import math
from REIXS_Detector_Offsets import OFFSETS

#mesh portion of the code might be broken
def ProcessScans(fname,scans):

  #Read the data and info from the spec file into lists
  sInfo,sData = ReadSpecFile(fname)
  if parameters.TYPE == "mesh":
    #An example of the fields available for a particular scan:
    print("\n\nFields available in sInfo:")
    for field in sInfo[scans[0][0]-1]: print(field)
    
    print("\n\nFields available in sData:")
    for field in sData[scans[0][0]-1]: print(field)
  
      #the edges between two meshes were double counted
      #for now we will remove the second count but averaging them is also an option
      #values defined in "SFO_Script"

      #find the indices of repeated values in each region
      repeats1 = []
      for index in range(parameters.TTH1_POINTS):
        repeats1.append(index*parameters.TH1_POINTS)
      repeats2 = []
      for index in range(parameters.TTH2_POINTS):
        repeats2.append(index*parameters.TH2_POINTS)

      #Note the spec scan numbers start at 1, but in sInfo and sData they start
      #at zero, so we need to account for this when accessing

      #loop through the meshes, merge them together, and save to filename identified by average temperature
      #also save total integrated intensity for each temperature
      temps = np.zeros((len(scans),3))
      for i in range(len(scans)):
        for j in range(len(scans[i])):
          scan = scans[i][j]
          if(j == 0):
            sensor = sData[scan-1][parameters.SENSOR] / sData[scan-1]["I0_BD3"]
            th = sData[scan-1]["Theta"]
            tth = sData[scan-1]["TwoTheta"] + offset
            temp = sData[scan-1]["Sample_T"]
          elif(j == 1):
            #repeat values are deleted
            sensor = np.hstack((sensor, np.delete(sData[scan-1][parameters.SENSOR] / sData[scan-1]["I0_BD3"], repeats1)))
            th = np.hstack((th, np.delete(sData[scan-1]["Theta"], repeats1)))
            tth = np.hstack((q, np.delete(sData[scan-1]["TwoTheta"], repeats1))) + offset
            temp = np.hstack((temp, np.delete(sData[scan-1]["Sample_T"], repeats1)))
          elif(j == 2):
            #repeat values are deleted
            sensor = np.hstack((sensor, np.delete(sData[scan-1][parameters.SENSOR] / sData[scan-1]["I0_BD3"], repeats2)))
            th = np.hstack((th, np.delete(sData[scan-1]["Theta"], repeats2)))
            tth = np.hstack((q, np.delete(sData[scan-1]["TwoTheta"], repeats2))) + offset
            temp = np.hstack((temp, np.delete(sData[scan-1]["Sample_T"], repeats2)))
            #####code below probably needs to be unindented?
            temps[i,0] = np.mean(temp)
            temps[i,1] = np.sum(sensor)
            temps[i,2] = np.mean(energy)
            np.savetxt(parameters.OUTPUT_LOCATION + parameters.OUTPUT_PREFIX + str(round(np.mean(temp),1)).zfill(5) + "K.dat",np.transpose(np.vstack((th,tth,sensor))))
            #maximums.append(max(PDInter))
            #print("temp: ", temps[i,0], ", max: ", max(PDInter))
    np.savetxt(parameters.OUTPUT_LOCATION + parameters.OUTPUT_PREFIX + "TDep.dat",temps)

  else:
    #An example of the fields available for a particular scan:
    print("\n\nFields available in sInfo:")
    for field in sInfo[scans[0]-1]:
      print(field)
    
    print("\n\nFields available in sData:")
    for field in sData[scans[0]-1]:
      print(field)
    #Note the spec scan numbers start at 1, but in sInfo and sData they start
    #at zero, so we need to account for this when accessing

    #loop through the scans and save to filename identified by average temperature
    #also save total integrated intensity for each temperature
    if parameters.MULTIPLE_ZONES != "true":
        temps = np.zeros((len(scans), 3))
        for i in range(len(scans)):
          scan = scans[i]
          sensor = sData[scan-1][parameters.SENSOR] / sData[scan-1]["I0_BD3"]
          #th = sData[scan-1]["Theta"]
          #tth = sData[scan-1]["TwoTheta"] + offset
          temp = sData[scan-1]["Sample_T"]
          #energy = sInfo[scan-1]["MonoEngy"]
          energy = sData[scan-1]["MonoEngy"]
          temps[i,0] = np.mean(temp)
          temps[i,1] = np.sum(sensor)
          temps[i,2] = np.mean(energy)
          #np.savetxt(parameters.OUTPUT_LOCATION + parameters.OUTPUT_PREFIX + str(round(np.mean(temp),1)).zfill(5) + "K.dat", np.transpose(np.vstack((th, tth, sensor))))
          np.savetxt(parameters.OUTPUT_LOCATION + parameters.OUTPUT_PREFIX + str(round(np.mean(temp),1)).zfill(5) + "K.dat", np.transpose(np.vstack((sensor, energy))))
          #maximums.append(max(PDInter))
          #print("temp: ", temps[i,0], ", max: ", max(PDInter))  
          np.savetxt(parameters.OUTPUT_LOCATION + parameters.OUTPUT_PREFIX + "TDep.dat",temps)
    else:
        temps = np.zeros((len(scans), 3))
        for i in range(len(scans)):
          scan = scans[i]
          sensor = sData[scan-1][parameters.SENSOR] / sData[scan-1]["I0_BD3"]
          th = sData[scan-1]["Theta"]
          tth = sData[scan-1]["TwoTheta"] + offset
          idx1 = (np.abs(np.asarray(tth) - parameters.ZONE1[0])).argmin()
          idx2 = (np.abs(np.asarray(tth) - parameters.ZONE1[1])).argmin()
          idx3 = (np.abs(np.asarray(tth) - parameters.ZONE2[0])).argmin()
          idx4 = (np.abs(np.asarray(tth) - parameters.ZONE2[1])).argmin()
          temp = sData[scan-1]["Sample_T"]
          energy = sInfo[scan-1]["MonoEngy"]
          temps[i,0] = np.mean(temp)
          temps[i,1] = np.sum(sensor)
          temps[i,2] = np.mean(energy)
          np.savetxt(parameters.OUTPUT_LOCATION_ZONE1 + parameters.OUTPUT_PREFIX + "ZONE1_" + str(round(np.mean(temp),1)).zfill(5) + "K.dat", np.transpose(np.vstack((th[idx1:idx2], tth[idx1:idx2], sensor[idx1:idx2]))))
          np.savetxt(parameters.OUTPUT_LOCATION_ZONE2 + parameters.OUTPUT_PREFIX + "ZONE2_" + str(round(np.mean(temp),1)).zfill(5) + "K.dat", np.transpose(np.vstack((th[idx3:idx4], tth[idx3:idx4], sensor[idx3:idx4]))))
          #maximums.append(max(PDInter))
          #print("temp: ", temps[i,0], ", max: ", max(PDInter))  
          np.savetxt(parameters.OUTPUT_LOCATION_ZONE1 + parameters.OUTPUT_PREFIX + "ZONE1_" + "TDep.dat",temps)
          np.savetxt(parameters.OUTPUT_LOCATION_ZONE2 + parameters.OUTPUT_PREFIX + "ZONE2_" + "TDep.dat",temps)


if __name__ == '__main__':
    parameters = __import__(sys.argv[1])
    print("Check that the detector offsets are correct for your measurements.")
    offset = OFFSETS[parameters.SENSOR]
    offset = offset - 10.9 #delete this when finished
    ProcessScans(parameters.DATA_FILE, parameters.RELEVANT_SCANS)


